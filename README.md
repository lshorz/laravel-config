# Laraver Config

## 一、安装
```php
composer require lshorz/laravel-config:"dev-master"
```
保证 *storage/cfg* 目录777

1、发布：
```php
php artisan vendor:publish --provider="Lshorz\LaravelConfig\CfgServiceProvider"
```

2、执行:
```php
php artisan optimize:clear
或
php artisan config:clear

php artisan migrate
```

## 二、使用说明
### 1、配置项参考文件
```php
config/cfg.php
```

### 2、例子
```php
//引入包
use Lshorz\LaravelConfig\CfgManager;

/**
 * 初始化
 * 
 * 注意：init() 方法必须要调用，
 * 如该方法不带参数，则默认采取config/cfg.php配置
 * 中的默认标识为config.cfg,
 * 
 * @var Lshorz\LaravelConfig\CfgHandlerInterface
 */
$config = app(CfgManager::class)->init();
```

#### 2.1 文件型驱动
```php
/**
 * 创建一个配置且会以该标识为文件名创建在cfg目录中
 */
$config->create([
            'name' => 'tester',
            'age' => 35,
            'score' => [
                'math' => 90,
                'english' => 85
            ]
        ]);

dump($config->all()); //获取该配置的所有项

//获取指定键值为name和score的配置值
dump($config->get(['name', 'score']));

//检查配置键值是否存在
$config->has('score.chinese');

//更新配置中键值为score.math的数据
$config->update('score.math', 100);

/**
 * 注意：非默认配置标识（config.cfg）
 * 则须给 init() 方法指定标识参数
 * 如：指定配置标识为test.cfg
 */
$config = app(CfgManager::class)->init('test.cfg');
//创建一个新的配置并指定存储到一个新的文件test.cfg
$config->create([...]); //创建配置

$config->all(); //获取所有配置

$config->destroy(); //删除配置
```

#### 2.2 database型驱动
```php
//在配置文件 *config/cfg.php* 设置
'driver' => 'database'
````
```php
/**
 * 创建一个名为test.cfg的配置,
 * 采用DB型驱动则配置标识作为表中的查询字段
 */
$config = app(CfgManager::class);
$config->init('test.cfg')->create([
            'name' => 'tester',
            'age' => 35,
            'score' => [
                'math' => 90,
                'english' => 85
            ]
        ]);

dump($config->init('test.cfg')->all()); //获取该配置的所有项

//获取配置中键值为name的值
dump($config->init('test.cfg')->get('name'));

//验证配置
if ($config->init('test.cfg')->valid()) { ... }

//删除配置
$config->init('test.cfg')->destroy();
```

#### 2.3 动态指定驱动
```php
//引入包
use Lshorz\LaravelConfig\CfgManager;
$cfg = app(CfgManager::class);

//使用文件型驱动创建配置并存储在abc.cfg中 
$cfg->driver('file')->init('abc.cfg')->create([...]);

//使用database型驱动
$cfg->driver('database')->init('abc.cfg')->update('name', '张三');
```

#### 2.4 数组式访问
所有驱动均继承CfgHandlerAbstract.php 抽象类，该类实现了ArrayAccess接口
```php
$config = app(CfgManager::class)->init();

//初始化后可以直接以数组方式访问        
dump($config['name']);
```

### 3、Facades例子
```php
//引用Facades
use Lshorz\LaravelConfig\Facades\Cfg;

Cfg::init('access.cfg')->all();

Cfg::driver('database')->init('config.cfg')->all();

Cfg::driver('file')->init('config.cfg')->valid();
```

### 4、使用中间件验证例子
```php
//引用中间件
Lshorz\LaravelConfig\Middleware\CfgVerify;

/**
 * 中间件参数为init()方法参数及driver()方法参数
 * 两个参数默认为null
 */
Route::middleware(CfgVerify::class . ":config.cfg,database")->group(function(){
    Route::get('test', [TestController::class, 'index']);
});
```

### 5、辅助函数
```php
/**
 * 指定驱动返回配置接口对象
 *
 * @param string $driver
 * @param string|null $identifier 文件名|配置名|keys
 * @return CfgHandlerInterface
 */
function cfg_driver(string $driver, ?string $identifier = null): CfgHandlerInterface

/**
 * 返回配置接口对象
 *
 * @param string|null $identifier 文件名|配置名|keys
 * @return CfgHandlerInterface
 */
function cfg(?string $identifier = null): CfgHandlerInterface

/**
 * 验证配置
 *
 * @param string|null $identifier 文件名|配置名|keys
 * @return bool
 */
function cfg_valid(?string $identifier = null): bool

/**
 * 获取配置列表
 *
 * @param string|null $identifier 文件名|配置名|keys
 * @return array
 */
function cfg_index(?string $identifier = null): array

/**
 * 创建配置
 *
 * @param array $items
 * @param string|null $identifier 文件名|配置名|keys
 * @return bool
 */
function cfg_create(array $items, ?string $identifier = null): bool

/**
 * 更新配置
 *
 * @param string|null $key
 * @param mixed $value
 * @param string|null $identifier 文件名|配置名|keys
 * @return bool
 */
function cfg_update(?string $key, $value, ?string $identifier = null): bool

/**
 * 删除配置
 *
 * @param string|null $identifier 文件名|配置名|keys
 * @return bool
 */
function cfg_destroy(?string $identifier = null): bool

/**
 * 检查配置中是否存在键值为$key的值
 *
 * @param string $key
 * @param string|null $identifier 文件名|配置名|keys
 * @return bool
 */
function cfg_has(string $key, ?string $identifier = null): bool

/**
 * 根据键值获取配置
 *
 * @param mixed $key
 * @param string|null $identifier 文件名|配置名|keys
 * @return mixed
 */
function cfg_get($key = null, ?string $identifier = null)

/**
 * 获取整个配置数组
 *
 * @param string|null $identifier 文件名|配置名|keys
 * @return array
 */
function cfg_all(?string $identifier = null): array
```


### 6、全部接口方法
```php
/**
 * 初始化
 *
 * @param string|null $identifier 配置标识
 * @return $this
 */
public function init(?string $identifier): self;

/**
 * 返回配置标识
 *
 * @return string
 */
public function getIdentifier(): string

/**
 * 验证配置
 *
 * @return bool
 */
public function valid(): bool;

/**
 * 获取配置列表
 *
 * @return array
 */
public function index(): array;

/**
 * 创建配置
 *
 * @param array $items
 * @return bool
 */
public function create(array $items): bool;

/**
 * 更新配置
 *
 * @param string|null $key
 * @param mixed $value
 * @return bool
 */
public function update(?string $key, $value): bool;

/**
 * 销毁配置
 *
 * @return bool
 */
public function destroy(): bool;

/**
 * 保存配置
 *
 * @return bool
 */
public function store(): bool;

/**
 * 检查配置中是否存在键值为$key的值
 *
 * @param string $key
 * @return bool
 */
public function has(string $key): bool;

/**
 * 根据键值获取配置
 *
 * @param array|string|null $key
 * @param mixed $default
 * @return mixed
 */
public function get($key = null, $default = null);

/**
 * 获取整个配置数组
 *
 * @return array
 */
public function all(): array;

/**
 * 设置配置值
 *
 * @param  array|string  $key
 * @param  mixed  $value
 * @return self
 */
public function set($key, $value = null): self;

/**
 * 将值插入到数组配置值前面
 *
 * @param  string  $key
 * @param  mixed  $value
 * @return self
 */
public function prepend(string $key, $value): self;

/**
 * 值插入到数组配置值后面
 *
 * @param  string  $key
 * @param  mixed  $value
 * @return self
 */
public function push(string $key, $value): self;
```

