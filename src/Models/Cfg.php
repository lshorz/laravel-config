<?php

namespace Lshorz\LaravelConfig\Models;

use Illuminate\Database\Eloquent\Model;

class Cfg extends Model
{
    protected $dates = ['created_at', 'updated_at'];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function getTable()
    {
        return config('cfg.database.table_name', parent::getTable());
    }

    public function getKey()
    {
        return config('cfg.database.primary_key', parent::getKey());
    }
}
