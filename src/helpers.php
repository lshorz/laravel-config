<?php

use Lshorz\LaravelConfig\CfgManager;
use Lshorz\LaravelConfig\CfgHandlerInterface;

if (!function_exists('cfg_driver')) {
    /**
     * 指定驱动返回配置接口对象
     *
     * @param string $driver
     * @param string|null $identifier 文件名|配置名|keys
     * @return CfgHandlerInterface
     */
    function cfg_driver(string $driver, ?string $identifier = null): CfgHandlerInterface
    {
        return app(CfgManager::class)->driver($driver)->init($identifier);
    }
}

if (!function_exists('cfg')) {
    /**
     * 返回配置接口对象
     *
     * @param string|null $identifier 文件名|配置名|keys
     * @return CfgHandlerInterface
     */
    function cfg(?string $identifier = null): CfgHandlerInterface
    {
        return app(CfgManager::class)->init($identifier);
    }
}

if (!function_exists('cfg_valid')) {
    /**
     * 验证配置
     *
     * @param string|null $identifier 文件名|配置名|keys
     * @return bool
     */
    function cfg_valid(?string $identifier = null): bool
    {
        return app(CfgManager::class)->init($identifier)->valid();
    }
}

if (!function_exists('cfg_index')) {
    /**
     * 获取配置列表
     *
     * @param string|null $identifier 文件名|配置名|keys
     * @return array
     */
    function cfg_index(?string $identifier = null): array
    {
        return app(CfgManager::class)->init($identifier)->index();
    }
}

if (!function_exists('cfg_create')) {
    /**
     * 创建配置
     *
     * @param array $items
     * @param string|null $identifier 文件名|配置名|keys
     * @return bool
     */
    function cfg_create(array $items, ?string $identifier = null): bool
    {
        return app(CfgManager::class)->init($identifier)->create($items);
    }
}

if (!function_exists('cfg_update')) {
    /**
     * 更新配置
     *
     * @param string|null $key
     * @param mixed $value
     * @param string|null $identifier 文件名|配置名|keys
     * @return bool
     */
    function cfg_update(?string $key, $value, ?string $identifier = null): bool
    {
        return app(CfgManager::class)->init($identifier)->update($key, $value);
    }
}

if (!function_exists('cfg_destroy')) {
    /**
     * 删除配置
     *
     * @param string|null $identifier 文件名|配置名|keys
     * @return bool
     */
    function cfg_destroy(?string $identifier = null): bool
    {
        return app(CfgManager::class)->init($identifier)->destroy();
    }
}

if (!function_exists('cfg_has')) {
    /**
     * 检查配置中是否存在键值为$key的值
     *
     * @param string $key
     * @param string|null $identifier 文件名|配置名|keys
     * @return bool
     */
    function cfg_has(string $key, ?string $identifier = null): bool
    {
        return app(CfgManager::class)->init($identifier)->has($key);
    }
}

if (!function_exists('cfg_get')) {
    /**
     * 根据键值获取配置
     *
     * @param mixed $key
     * @param string|null $identifier 文件名|配置名|keys
     * @return mixed
     */
    function cfg_get($key = null, ?string $identifier = null)
    {
        return app(CfgManager::class)->init($identifier)->get($key, null);
    }
}

if (!function_exists('cfg_all')) {
    /**
     * 获取整个配置数组
     *
     * @param string|null $identifier 文件名|配置名|keys
     * @return array
     */
    function cfg_all(?string $identifier = null): array
    {
        return app(CfgManager::class)->init($identifier)->all();
    }
}

