<?php
declare(strict_types=1);

namespace Lshorz\LaravelConfig;

interface CfgHandlerInterface
{
    /**
     * 初始化
     *
     * @param string|null $identifier 配置标识
     * @return $this
     */
    public function init(?string $identifier = null): self;

    /**
     * 返回配置标识
     *
     * @return string
     */
    public function getIdentifier(): string;

    /**
     * 验证配置
     *
     * @return bool
     */
    public function valid(): bool;

    /**
     * 获取配置列表
     *
     * @return array
     */
    public function index(): array;

    /**
     * 创建配置
     *
     * @param array $items
     * @return bool
     */
    public function create(array $items): bool;

    /**
     * 更新配置
     *
     * @param string|null $key
     * @param mixed $value
     * @return bool
     */
    public function update(?string $key, $value): bool;

    /**
     * 销毁配置
     *
     * @return bool
     */
    public function destroy(): bool;

    /**
     * 保存配置
     *
     * @return bool
     */
    public function store(): bool;

    /**
     * 检查配置中是否存在键值为$key的值
     *
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool;

    /**
     * 根据键值获取配置
     *
     * @param array|string|null $key
     * @param mixed $default
     * @return mixed
     */
    public function get($key = null, $default = null);

    /**
     * 获取整个配置数组
     *
     * @return array
     */
    public function all(): array;

    /**
     * 设置配置值
     *
     * @param  array|string  $key
     * @param  mixed  $value
     * @return self
     */
    public function set($key, $value = null): self;

    /**
     * 将值插入到数组配置值前面
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return self
     */
    public function prepend(string $key, $value): self;


    /**
     * 值插入到数组配置值后面
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return self
     */
    public function push(string $key, $value): self;
}
