<?php

namespace Lshorz\LaravelConfig\Facades;

use Illuminate\Support\Facades\Facade;


class Cfg extends Facade
{
    /**
     * @method static $this driver(string $driver)
     * @method static $this init(?string $identifier)
     * @method static $this valid()
     * @method static $this index()
     * @method static $this create(array $items)
     * @method static $this update(?string $key, $items)
     * @method static $this destroy()
     * @method static $this has(string $key)
     * @method static $this get(mixed $key, mixed $default = null)
     * @method static $this all()
     */
    protected static function getFacadeAccessor()
    {
        return 'Cfg';
    }
}
