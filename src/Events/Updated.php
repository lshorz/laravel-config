<?php
declare(strict_types=1);
/**
 * 配置更新事件
 */

namespace Lshorz\LaravelConfig\Events;

class Updated
{
    /**
     * 配置名
     */
    public string $name;

    /**
     * 配置更新结果
     */
    public array $result;

    public function __construct(string $identifier, array $result)
    {
        $this->name = $identifier;
        $this->result = $result;
    }
}
