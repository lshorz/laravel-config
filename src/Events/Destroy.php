<?php
declare(strict_types=1);

/**
 * 配置删除事件
 */
namespace Lshorz\LaravelConfig\Events;

class Destroy
{
    /**
     * 配置名
     */
    public string $name;

    public function __construct(string $identifier)
    {
        $this->name = $identifier;
    }
}
