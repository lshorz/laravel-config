<?php
declare(strict_types=1);

namespace Lshorz\LaravelConfig;

use Illuminate\Support\Manager;

/**
 * @method $this driver(string $driver)
 * @method CfgHandlerInterface init(?string $identifier)
 * @method CfgHandlerInterface getIdentifier()
 * @method CfgHandlerInterface valid()
 * @method CfgHandlerInterface index()
 * @method CfgHandlerInterface create(array $items)
 * @method CfgHandlerInterface update(?string $key, $items)
 * @method CfgHandlerInterface destroy()
 * @method CfgHandlerInterface store()
 * @method CfgHandlerInterface has(string $key)
 * @method CfgHandlerInterface get(mixed $key, mixed $default = null)
 * @method CfgHandlerInterface all()
 *
 */
class CfgManager extends Manager
{
    public function getDefaultDriver(): string
    {
        return $this->config->get('cfg.driver');
    }

    /**
     * 文件配置驱动
     *
     * @return CfgHandlerInterface
     */
    protected function createFileDriver(): CfgHandlerInterface
    {
        return new FileCfgHandler($this->config, $this->container->make('files'));
    }

    /**
     * DB配置驱动
     *
     * @return CfgHandlerInterface
     */
    protected function createDatabaseDriver(): CfgHandlerInterface
    {
        return new DatabaseCfgHandler($this->config);
    }
}
