<?php

return [
    //驱动可选[file|database]
    'driver' => env('CONFIG_DRIVER', 'file'), //默认驱动为file
    'default_identifier' => env('CFG_DEFAULT_IDENTIFIER', 'config.cfg'), //默认配置标识

    //文件驱动配置
    'file' => [
        'path' => storage_path('cfg') //配置文件存储路径
    ],

    //database驱动配置
    'database' => [
        'models' => Lshorz\LaravelConfig\Models\Cfg::class, //对应的配置模型
        'table_name' => 'cfg', //配置表名
        'primary_key' => 'id', //主键
        'field_identifier' => 'identifier', //配置标识的字段名
        'field_value' => 'value', //配置内容的字段名
    ],

    'cache' => [
        'open' => env('CFG_CACHE_OPEN', true), //是否开启配置缓存
        'key_prefix' => env('CFG_CACHE_PREFIX', 'cfg_cache_'), //缓存键值前缀
        'ttl' => env('CFG_CACHE_TTL', -1) //单位秒,-1:则永久缓存
    ],
];